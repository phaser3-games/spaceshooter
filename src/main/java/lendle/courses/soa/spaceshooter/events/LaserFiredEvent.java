/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.spaceshooter.events;

import lendle.courses.soa.spaceshooter.Velocity;

/**
 *
 * @author lendle
 */
public class LaserFiredEvent {
    private int x, y;
    private Velocity velocity=null;
    private String id=null, firedBy=null;

    public String getFiredBy() {
        return firedBy;
    }

    public void setFiredBy(String firedBy) {
        this.firedBy = firedBy;
    }

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Velocity getVelocity() {
        return velocity;
    }

    public void setVelocity(Velocity velocity) {
        this.velocity = velocity;
    }
    
}
