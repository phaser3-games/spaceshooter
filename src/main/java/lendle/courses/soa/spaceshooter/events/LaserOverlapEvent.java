/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.spaceshooter.events;

/**
 *
 * @author lendle
 */
public class LaserOverlapEvent {
    private String rocketId=null;
    private String laserId=null;
    private String laserFiredBy=null;

    public String getLaserId() {
        return laserId;
    }

    public void setLaserId(String laserId) {
        this.laserId = laserId;
    }

    
    public String getRocketId() {
        return rocketId;
    }

    public void setRocketId(String rocketId) {
        this.rocketId = rocketId;
    }

    public String getLaserFiredBy() {
        return laserFiredBy;
    }

    public void setLaserFiredBy(String laserFiredBy) {
        this.laserFiredBy = laserFiredBy;
    }
    
}
