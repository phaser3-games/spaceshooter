/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.spaceshooter.events;

/**
 *
 * @author lendle
 */
public class RocketUpdateEvent {
    private String id=null;
    private double x=-1;
    private double y=-1;
    private double angle=-1;
    private long tint=-1;

    public long getTint() {
        return tint;
    }

    public void setTint(long tint) {
        this.tint = tint;
    }
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }
    
    
}
