/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lendle.courses.soa.spaceshooter;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;
import java.util.HashMap;
import java.util.Map;
import lendle.courses.soa.spaceshooter.events.LaserFiredEvent;
import lendle.courses.soa.spaceshooter.events.LaserOverlapEvent;
import lendle.courses.soa.spaceshooter.events.RocketShotEvent;
import lendle.courses.soa.spaceshooter.events.RocketUpdateEvent;

/**
 *
 * @author lendle
 */
public class MainServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Configuration config = new Configuration();
        config.setPort(8000);
        SocketIOServer server = new SocketIOServer(config);
        Map<String, RocketPeer> rocketPeers = new HashMap<>();

        server.addEventListener("rocketUpdate", RocketUpdateEvent.class, new DataListener<RocketUpdateEvent>() {
            @Override
            public void onData(SocketIOClient sioc, RocketUpdateEvent t, AckRequest ar) throws Exception {
                System.out.println(t.getId()+":"+t.getX()+":"+t.getY()+":"+t.getAngle()+":"+sioc.getSessionId());
                String id = t.getId();
                RocketPeer peer = rocketPeers.get(id);
                if (peer == null) {
                    peer = new RocketPeer();
                    peer.setId(id);
                    rocketPeers.put(id, peer);
                }
                peer.setX(t.getX());
                peer.setY(t.getY());
                peer.setAngle(t.getAngle());
                server.getBroadcastOperations().sendEvent("rocketUpdate", t);
            }

        });
        server.addEventListener("laserFired", LaserFiredEvent.class, new DataListener<LaserFiredEvent>() {
            @Override
            public void onData(SocketIOClient sioc, LaserFiredEvent t, AckRequest ar) throws Exception {
                System.out.println("laserFired: "+t.getId());
                server.getBroadcastOperations().sendEvent("laserFired", t);
            }
        });
        server.addEventListener("laserOverlapped", LaserOverlapEvent.class, new DataListener<LaserOverlapEvent>() {
            @Override
            public void onData(SocketIOClient sioc, LaserOverlapEvent t, AckRequest ar) throws Exception {
                if (t.getRocketId().equals(t.getLaserFiredBy()) == false) {
                    RocketPeer rocketPeer = rocketPeers.get(t.getRocketId());
                    if(rocketPeer.isAlive()){
                        rocketPeer.setAlive(false);
                        RocketShotEvent rocketShotEvent=new RocketShotEvent();
                        rocketShotEvent.setLaserId(t.getLaserId());
                        rocketShotEvent.setRocketId(t.getRocketId());
                        rocketShotEvent.setFiredBy(t.getLaserFiredBy());
                        server.getBroadcastOperations().sendEvent("rocketShot", rocketShotEvent);
                    }
                }
            }
        });
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                server.stop();
            }
        });
        server.start();
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (Throwable e) {
                e.printStackTrace();
                server.stop();
                break;
            }
        }

    }

}
