import Phaser from 'phaser'

class Laser extends Phaser.Physics.Arcade.Sprite {
  constructor (scene, x, y, id, velocity, firedBy) {
    super(scene, x, y, 'laser');
    scene.add.existing(this);
    scene.physics.world.enable(this);
    this.body.setGravity(0);
    this.velocity=velocity;
    this.id=id;
    this.firedBy=firedBy;
  }

  update () {
    this.setVelocity(this.velocity.x, this.velocity.y);
  }
}
export {Laser};