import Phaser from 'phaser'

class Logo extends Phaser.GameObjects.Image {
  constructor (scene, x, y) {
    super(scene, x, y, 'logo')
    scene.add.existing(this)
  }
}

export {Logo};
