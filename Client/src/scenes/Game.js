import Phaser from 'phaser'
import { Rocket } from '../sprites/Rocket.js'
import { Laser } from '../sprites/Laser.js'
import io from 'socket.io-client'

class Game extends Phaser.Scene {
  constructor() {
    super({ key: 'Game' })
    this.lives = 1;
    this.ammo = 5;
    this.score = 0;
    this.lastState = {
      x: -1, y: -1, angle: -1
    };
  }

  create() {
    this.flag=Math.floor(Math.random()*16777215);
    document.getElementById("flag").style.backgroundColor="#"+this.flag.toString(16);
    this.socket = io("http://localhost:8000");
    let self = this;
    this.id = "" + new Date().getTime()+Math.random();
    this.rocket = new Rocket(this, this.id, Math.random()*1000, Math.random()*600, this.flag);
    this.cursors = this.input.keyboard.createCursorKeys();
    this.lasers = this.physics.add.group();
    this.otherRockets = {};
    this.pendingUpdates = [];
    this.allLasers={};
    this.rockets=this.physics.add.group();
    this.rockets.add(this.rocket);

    this.socket.on('rocketUpdate', function (e) {
      if (!self.otherRockets[e.id] && e.id!=self.id) {
        let otherRocket = new Rocket(self, e.id, e.x, e.y, e.tint);
        self.otherRockets[e.id] = otherRocket;
        self.rockets.add(otherRocket);
        self.pendingUpdates.push(e);
      }else{
        let rocket=(self.rocket.id==e.id)?self.rocket:self.otherRockets[e.id];
        rocket.x=e.x;
        rocket.y=e.y;
        rocket.angle=e.angle;
      }
    });

    this.socket.on('laserFired', function(e){
      if(!self.allLasers[e.id]){
        let laser=new Laser(self, e.x, e.y, e.id, e.velocity, e.firedBy);
        self.allLasers[e.id]=laser;
        self.lasers.add(laser);
      }
    });
    this.socket.on("rocketShot", function(e){
      let rocket=(self.rocket.id==e.rocketId)?self.rocket:self.otherRockets[e.rocketId];
      rocket.disableBody(true, true);
      self.allLasers[e.laserId].disableBody(true, true);
    });

    this.fireMoveEvent();
    this.physics.add.overlap(this.rockets, this.lasers, function(o1, o2){
      if(o2.firedBy!=o1.id){
        self.socket.emit('laserOverlapped', {
          rocketId: o1.id,
          laserFiredBy: o2.firedBy,
          laserId: o2.id
        });
      }
    });
  }

  fireMoveEvent(){
    this.updateLastState();
    this.socket.emit('rocketUpdate', this.lastState);
  }

  updateLastState(){
    this.lastState = {
      "id": this.rocket.id,
      "x": this.rocket.x,
      "y": this.rocket.y,
      "angle": this.rocket.angle,
      "tint": this.rocket.flag
    };
  }

  update() {
    this.rocket.idle();
    if (this.cursors.left.isDown) {
      this.rocket.goLeft();
    } else if (this.cursors.right.isDown) {
      this.rocket.goRight();
    }
    if (this.cursors.up.isDown) {
      this.rocket.doAccelerate();
    } else if (this.cursors.down.isDown) {
      this.rocket.doDeaccelerate();
    }

    if (this.cursors.space.isDown) {
      if (this.ammo > 0) {
        let laser = this.rocket.shoot();
        if (laser != null) {
          this.lasers.add(laser);
          laser.local=true;
          this.ammo--;
          document.getElementById("ammo").value = "" + this.ammo;
          this.allLasers[laser.id]=laser;
          this.socket.emit("laserFired", {
            id: laser.id,
            x: laser.x,
            y:laser.y,
            velocity: laser.velocity,
            firedBy: laser.firedBy
          });
        }
      }
    }
    this.lasers.children.iterate(function (laser) {
      laser.update();
      if (laser.x < 0 || laser.x > 1000 || laser.y < 0 || laser.y > 600) {
        laser.disableBody(true, true);
      }
    });
    this.rocket.update();
    if (this.rocket.x != this.lastState.x || this.rocket.y != this.lastState.y || this.rocket.angle != this.lastState.angle) {
      this.fireMoveEvent();
    }else{
      this.updateLastState();
    }
    
    for (let msg of this.pendingUpdates) {
      let otherRocket = this.otherRockets[msg.id];
      otherRocket.update(msg);
    }
    this.pendingUpdates = [];
  }
}

export { Game };
