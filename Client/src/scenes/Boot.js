import Phaser from 'phaser'
import rocket from 'images/pixel_ship_red_small_2.png'
import laser from 'images/pixel_laser_small_blue.png'
import asteroid from 'images/asteroid_grey_tiny.png'
import gold from 'images/asteroid_tiny.png'
import rocket_collide from 'images/rocket_collide.png'

class Boot extends Phaser.Scene {
  constructor () {
    super({ key: 'Boot' })
  }

  preload () {
    this.load.image('rocket', rocket)
    this.load.image('laser', laser)
    this.load.image('asteroid', asteroid)
    this.load.image('gold', gold)
    this.load.spritesheet('rocket_collide_anim', rocket_collide, { frameWidth: 70, frameHeight: 50 });
  }

  create () {
    this.scene.start('Game')
  }
}

export {Boot};